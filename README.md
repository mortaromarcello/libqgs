# README #

This is a wrapper for Ghostscript library which uses the Qt toolkit. 
Please check the documentation in the qgs.h header.

This library is licenced under LGPL but using the GPL headers from 
GPL Ghostscript 9.05 make it tighten the conditions to GPL.

This library is part of a project sponsored by Google's Summer of Code program,
http://code.google.com/summerofcode.html.
