/***************************************************************************
 *   Copyright (C) 2005 by Piotr Szymanski <niedakh@gmail.com>             *
 *                                                                         *
 * This library is free software; you can redistribute it and/or           *
 * modify it under the terms of the GNU Lesser General Public              *
 * License as published by the Free Software Foundation; either            *
 * version 2.1 of the License, or (at your option) any later version.      *
 *                                                                         *
 * This library is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       *
 * Lesser General Public License for more details.                         *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with this library; if not, write to the Free Software     *
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,                  *
 * MA  02110-1301  USA                                                     *
 ***************************************************************************/

#include <qstringlist.h>
#include <qapplication.h>
#include "qgs.h"
/*
 *	Example use of the buffer.
 */ 
int main (int argc, char* argv[])
{
	QApplication(argc,argv);
	GSInterpreterLib interpreter;
	QStringList arg;
        arg << " "
	<< "-sDEVICE=ziew";
	interpreter.setDisplay(false);
	interpreter.setGhostscriptArguments(arg);
	try
	{
		interpreter.setBuffered(true);
		interpreter.start(true);
	}
	catch (GSInterpreterLib::GSError e)
	{
		if (interpreter.IOBuffer().contains("Unknown device"));
			qDebug("Device not found");
	};
	return 0;
}
