/***************************************************************************
 *   Copyright (C) 2005 by Piotr Szymanski <niedakh@gmail.com>             *
 *                                                                         *
 * This library is free software; you can redistribute it and/or           *
 * modify it under the terms of the GNU Lesser General Public              *
 * License as published by the Free Software Foundation; either            *
 * version 2.1 of the License, or (at your option) any later version.      *
 *                                                                         *
 * This library is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       *
 * Lesser General Public License for more details.                         *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with this library; if not, write to the Free Software     *
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,                  *
 * MA  02110-1301  USA                                                     *
 ***************************************************************************/

#include <QStringList>
#include <QApplication>
#include <QDebug>
#include "qgs.h"
#include <iostream>

/*
 *	Example of not using the GUI and the default arguments - getting a DSC document describing a PDF.
 */
int main (int argc, char* argv[])
{
	QApplication(argc,argv);
	if (argc<3)
	{
		qDebug() << "Usage " << QString(argv[0]) << " PDFfile PSFile\n";
		return -1;
	}
	GSInterpreterLib interpreter;
	QStringList arg;
        arg << " "
        << "-q"
	<< "-dNOPAUSE"
	<< "-dBATCH"
	<< "-dSAFER"
	<< "-sDEVICE=ps2write"
	<< QString("-sOutputFile=%1").arg(argv[2])
        << "-f"
	<< QString("%1").arg(argv[1])
	;
	interpreter.setDisplay(false);
	interpreter.setGhostscriptArguments(arg);
	interpreter.start(false);
	return 0;
}
