/***************************************************************************
 *   Copyright (C) 2005 by Piotr Szymanski <niedakh@gmail.com>             *
 *                                                                         *
 * This library is free software; you can redistribute it and/or           *
 * modify it under the terms of the GNU Lesser General Public              *
 * License as published by the Free Software Foundation; either            *
 * version 2.1 of the License, or (at your option) any later version.      *
 *                                                                         *
 * This library is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       *
 * Lesser General Public License for more details.                         *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with this library; if not, write to the Free Software     *
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,                  *
 * MA  02110-1301  USA                                                     *
 ***************************************************************************/

#include "qgs.h"
#include <qapplication.h>

/*
 *	Example use of the exceptions, triggering the easiest error.
 */

int main (int argc, char* argv[])
{
    QApplication app(argc,argv);
	try 
	{
		// One cannot have two instances of ghostscript in one process
		GSInterpreterLib one;
		GSInterpreterLib two;
	}
	catch (GSInterpreterLib::GSError e)
	{
		qDebug("%s",e.name.toLatin1().data());
	};
	return 0;
}

