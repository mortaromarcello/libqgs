/***************************************************************************
 *   Copyright (C) 2005 by Piotr Szymanski <niedakh@gmail.com>             *
 *                                                                         *
 * This library is free software; you can redistribute it and/or           *
 * modify it under the terms of the GNU Lesser General Public              *
 * License as published by the Free Software Foundation; either            *
 * version 2.1 of the License, or (at your option) any later version.      *
 *                                                                         *
 * This library is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       *
 * Lesser General Public License for more details.                         *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with this library; if not, write to the Free Software     *
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,                  *
 * MA  02110-1301  USA                                                     *
 ***************************************************************************/

#include <math.h>
#include <QPixmap>
#include <QPainter>
#include <QString>
#include <QRect>
#include <cstdio>
#include <QtGlobal>
#include <QX11Info>
#include <ghostscript/iapi.h>
#include <ghostscript/ierrors.h>
#include <ghostscript/gdevdsp.h>
#include "qgs.h"

namespace GSApiWrapper
{
// C API wrappers
int handleStdin(void *caller_handle, char *buf, int len)
{
	if(caller_handle != 0)
		return static_cast <GSInterpreterLib*>(caller_handle) -> gs_input(buf,len);
	return 0;
}

int handleStdout(void *caller_handle, const char *str, int len)
{
	if(caller_handle != 0)
    	return static_cast <GSInterpreterLib*>(caller_handle) -> gs_output(str,len);
	return 0;
}

int handleStderr(void *caller_handle, const char *str, int len)
{
	if(caller_handle != 0)
		return static_cast <GSInterpreterLib*>(caller_handle) -> gs_error(str,len);
	return 0;
}

int open(void* handle, void* device)
{
    qDebug("open called");
	if (handle != 0)
		return static_cast <GSInterpreterLib*>(handle) -> open(device);
    return 0;
}

int preclose(void * handle, void * device )
{
    qDebug("preclose called");
    if (handle != 0)
		return static_cast <GSInterpreterLib*>(handle) -> preclose(device);
    return 0;
}

int close(void * handle, void * device )
{
    qDebug("close called");
	if (handle != 0)
		return static_cast <GSInterpreterLib*>(handle) -> close(device);
    return 0;
}


int presize(void * handle, void * device, int width, int height,
        int raster, unsigned int format)
{
    qDebug("presize called, width/height/raster: %d/%d/%d",width,height,raster);
	if (handle != 0)
		return static_cast <GSInterpreterLib*>(handle) -> presize(device, width, height,
        raster, format);
    return 0;
}


int size(void *handle, void *device, int width, int height,
        int raster, unsigned int format, unsigned char *pimage)
{
    qDebug("size called, width/height/raster: %d/%d/%d",width,height,raster);
	if (handle != 0)
    	return static_cast <GSInterpreterLib*>(handle) -> size (device, width, height,
        raster, format, pimage);
	return 0;
}

int sync(void * handle, void * device  )
{
    qDebug("sync called");
	if (handle != 0)
    	return static_cast <GSInterpreterLib*>(handle) -> sync(device);
    return 0;
}


int page(void *handle, void * device, int copies, int flush)
{
	qDebug("page called: copies/flush = %d/%d",copies,flush);
	if (handle != 0)
	    return static_cast <GSInterpreterLib*>(handle) -> page (device, copies, flush);
	return 0;
}

int update(void * handle, void * device,
    int x, int y, int w, int h)
{
	if (handle != 0)
	{
		GSInterpreterLib* tmp=static_cast <GSInterpreterLib*>(handle);
		// check it here, because this function is often executed by libgs, we dont 
		// want to waste performance
		if (tmp->progressive())
		{
// 			too much buzz
// 			qDebug("update called: (%d,%d)x(%d,%d)",x,y,w,h);
			return static_cast <GSInterpreterLib*>(handle) -> update(device, x, y, w, h);
		}
	}
    return 0;
}

int separation(void * handle, void * device,
    int comp_num, const char * name,
    unsigned short c, unsigned short m,
    unsigned short y, unsigned short k)
{
    qDebug("separation called");
	if (handle != 0)
		return static_cast <GSInterpreterLib*>(handle) -> separation(device, comp_num, name, c,m,y,k);
    return 0;
}

display_callback device =
    {
        sizeof ( display_callback ),
        DISPLAY_VERSION_MAJOR,
        DISPLAY_VERSION_MINOR,
        &open,
        &preclose,
        &close,
        &presize,
        &size,
        &sync,
        &page,
        &update,
        NULL,       /* memalloc */
        NULL
#if DISPLAY_VERSION_MAJOR  >= 2
        ,&separation
#endif
    };
}

/********* GSInterpreterLib ************/


GSInterpreterLib::GSInterpreterLib() : 
	m_running(false),
	m_sync(false),
	m_syncDone(false),
	m_orientation(0),
	m_magnify(1.0),
	m_width(0),
	m_height(0),
	m_media(QString::null),
	m_Gwidth(0),
	m_Gheight(0),
	m_imageChar(0),
	m_format(0),
	m_raster(0),
	m_textaa(1),
	m_graphaa(1),
	m_progr(false),
	m_pfonts(true),
	m_display(true),
	m_wasSize(false),
	m_wasPage(false),
	m_img(0),
	m_argsCCount(0),
	m_argsChar(0)
{
    int exit = gsapi_new_instance((void**)&ghostScriptInstance,this);
    qDebug("GS instance created with code: %d",exit);
	if (exit)
		handleExit(exit);
}

GSInterpreterLib::~GSInterpreterLib()
{
    if (running())
        gsapi_exit(ghostScriptInstance);
    if (locked())//interpreterLock.locked())
	interpreterLock.unlock();
    if (m_argsChar)
    {
        for (int i=0;i<m_argsCCount;i++)
            delete [] *(m_argsChar+i);
        delete [] m_argsChar;
    }
    gsapi_delete_instance(ghostScriptInstance);
	delete m_img;
}


// interpreter state functions 

bool GSInterpreterLib::start(bool setStdio)
{
    m_sync=false;
    qDebug("setting m_sync to %d in start",m_sync);
    if ( setStdio )
        gsapi_set_stdio (ghostScriptInstance,&(GSApiWrapper::handleStdin),
        &(GSApiWrapper::handleStdout),&(GSApiWrapper::handleStderr));
    qDebug("setting display");
    int call = gsapi_set_display_callback(ghostScriptInstance, &(GSApiWrapper::device));
	if (call)
		handleExit(call);
    qDebug("converting args to char**");
    argsToChar();
    qDebug("setting args");
    call=gsapi_init_with_args (ghostScriptInstance,m_argsCCount,m_argsChar);
	if (call)
		handleExit(call);
	QString set;
	set.sprintf("<< /Orientation %d >> setpagedevice .locksafe",m_orientation);
	gsapi_run_string_with_length (ghostScriptInstance,set.toLatin1(),set.length(),0,&call);
    m_running = handleExit ( call ) ;
    return m_running;
}

bool GSInterpreterLib::stop()
{
    if (m_running)
    {
        int exit=gsapi_exit(ghostScriptInstance);
		if (exit)
			handleExit(exit);
        qDebug("Ghostscript stopped in stop with code %d",exit);
        m_running=false;
        m_sync=false;
		qDebug("setting m_sync to %d in stop", m_sync);
    }
    return m_running;
}


// set options
void GSInterpreterLib::setGhostscriptArguments( const QStringList &list )
{
    interpreterLock.lock();
    if ( m_args != list )
    {
        m_args=list;
        stop();
    }
    interpreterLock.unlock();
}

void GSInterpreterLib::setOrientation( int orientation )
{
    interpreterLock.lock();
    if( m_orientation != orientation )
    {
        m_orientation = orientation;
        stop();
    }
    interpreterLock.unlock();
}

void GSInterpreterLib::setMagnify( double magnify )
{
    interpreterLock.lock();
    if( m_magnify != magnify )
    {
        m_magnify = magnify;
        stop();
    }
    interpreterLock.unlock();
}

void GSInterpreterLib::setMedia( const QString &media )
{
    interpreterLock.lock();
    if( m_media != media )
    {
        m_media = media;
        stop();
    }
    interpreterLock.unlock();
}

void GSInterpreterLib::setSize( int w, int h )
{
	interpreterLock.lock();
    if ( m_width != w ) 
    {
        m_width=w;
        stop();

    }
    if ( m_height != h )
    {
        m_height=h;
        stop();
    }
    interpreterLock.unlock();
}

void GSInterpreterLib::setDisplay( bool display )
{
    interpreterLock.lock();
    if( m_display != display )
    {
        m_display = display;
        stop();
    }
    interpreterLock.unlock();
}

void GSInterpreterLib::setPlatformFonts( bool pfonts )
{
    interpreterLock.lock();
    if( m_pfonts != pfonts )
    {
        m_pfonts = pfonts;
        stop();
    }
    interpreterLock.unlock();
}

void GSInterpreterLib::setProgressive( bool progr )
{
    interpreterLock.lock();
    if( m_progr != progr )
    {
        m_progr = progr;
        stop();
    }
    interpreterLock.unlock();
}

void GSInterpreterLib::setAABits(int text, int graphics )
{
    interpreterLock.lock();
    if( m_textaa != text )
    {
        m_textaa = text;
        stop();
    }
    if( m_graphaa != graphics )
    {
        m_graphaa = graphics;
        stop();
    }
    interpreterLock.unlock();
}


void GSInterpreterLib::setBuffered( bool buffered )
{
    interpreterLock.lock();
    if( m_buffered != buffered )
        m_buffered = buffered;
    interpreterLock.unlock();
}

bool GSInterpreterLib::run(FILE * tmp, Position pos, bool sync)
{
    if (fseek(tmp,pos.first, SEEK_SET))
        return false;
	if (!running())
		start();
    interpreterLock.lock();
    m_syncDone=false;
    m_sync=sync;
	m_wasPage=false;
    qDebug("setting m_sync to %d in run", m_sync);
    char buf [4096];
    int read, exit_code, wrote=0, left=pos.second - pos.first;
	if (progressive() && m_sync )
	{
		qDebug("Making image Gw/Gh %d/%d",m_Gwidth,m_Gheight);
		if ( m_img != 0 )
			qDebug("Possible memory leak, m_img is initialised before run");
		m_img=new QImage(m_Gwidth, m_Gheight, QImage::Format_RGB32);//32), 0, QImage::BigEndian );
		m_img->invertPixels();
		emit(Started(m_img));
	}
    gsapi_run_string_begin (ghostScriptInstance, 0, &exit_code);
	if (exit_code) handleExit(exit_code);
	qDebug("Left to write: %d",left);
    while (left > 0)
    {
        read=fread (buf,sizeof(char),qMin((int)sizeof(buf),left),tmp);
        wrote=gsapi_run_string_continue (ghostScriptInstance, buf, read, 0, &exit_code);
        if (exit_code) handleExit(exit_code);
        left-=read;
		qDebug("Left/read/wrote: %d/%d/%d",left,read,wrote);
    }
    gsapi_run_string_end (ghostScriptInstance, 0, &exit_code);
	if (exit_code) handleExit(exit_code);
    qDebug("unlocking interpreter with %d left unread",left);
	m_syncDone=false;
    interpreterLock.unlock();
    return true;
}



// gs api wrapping 
int GSInterpreterLib::gs_input  ( char* buffer, int len )
{
    emit io (Input,buffer,len);
    return len;
}
int GSInterpreterLib::gs_output ( const char* buffer, int len )
{
	if (m_buffered)
		m_buffer.append(QString::fromLocal8Bit (buffer,len));
    emit io (Output,buffer,len);
    return len;
}

int GSInterpreterLib::gs_error  ( const char* buffer, int len )
{
	if (m_buffered)
		m_buffer.append(QString::fromLocal8Bit (buffer,len));
    emit io (Error,buffer,len);
    return len;
}

int GSInterpreterLib::presize(void * /* device*/, int width, int height,
        	int raster, unsigned int format)
{
	m_Gwidth=width;
	m_Gheight=height;
    m_raster=raster;
    m_format=format;
	return 0;
}

int GSInterpreterLib::size(void * /* device*/, int /*width*/, int /*height*/,
        int /*raster*/, unsigned int /*format*/, unsigned char *pimage)
{
	m_wasSize=true;
    m_imageChar=pimage;
	// make it white
    return 0;
}

int GSInterpreterLib::update(void * /* device*/, int x, int y, int w, int h)
{
	if (m_wasSize && !m_wasPage && m_imageChar!=0 && m_img!=0)
	{
		unsigned char *s;
		int th=y+h,tw=x+w;
		for (int i=y;i<th;i++)
		{
			for (int j=x;j<tw;j++)
			{
				s = m_imageChar + (i*m_raster) + j*4;
				uint* p= reinterpret_cast<uint*> (m_img->scanLine(i) + j*4);
				*p=qRgb( (uint) s[0], (uint) s[1], (uint) s[2] );
			}
		}
		QRect t(x,y,w,h);
		emit Updated(t);
	}
// 	else
// 		qDebug("U: %d %d %d %d",x,y,w,h);
    return 0;
}

#include <qdialog.h>
#include <qlabel.h>
#include <qpixmap.h>

int GSInterpreterLib::page(void * /* device*/, int /* copies*/, int /* flush*/)
{
	m_wasPage=true;
	qDebug("setting m_sync to %d in page", m_sync);
    if (m_sync && !m_syncDone)
    {
		// assume the image given by the gs is of the requested widthxheight
		if (! progressive() )
		{
			m_img=new QImage(m_imageChar, m_Gwidth, m_Gheight,QImage::Format_RGB32);//, (QRgb*) 0, 0,  QImage::BigEndian );
			interpreterLock.unlock();
			qDebug("Finished nonprogressive: %dx%d int %d",m_Gwidth,m_Gheight,locked());//interpreterLock.locked());
			emit Finished(m_img);
		}
		else
		{
			interpreterLock.unlock();
			qDebug("Finished progressive: %dx%d int %d",m_Gwidth,m_Gheight,locked());//interpreterLock.locked());
			emit Finished();
		}
		m_syncDone=true;
    }
    return 0;
}

bool GSInterpreterLib::handleExit(int code)
{
	qWarning("Got code %d",code);
	if ( code>=0 )
		return true;
	else if ( code <= -100 )
	{
		GSError e;
		switch (code)
		{
			case e_Fatal:
				e.code=e_Fatal;
				e.name="fatal internal error";
				if (locked())//interpreterLock.locked())
					interpreterLock.unlock();
				throw e;
				break;

			case e_ExecStackUnderflow:
				e.code=e_ExecStackUnderflow;
				e.name="stack overflow";
				if (locked())//interpreterLock.locked())
					interpreterLock.unlock();
				throw e;
				break;

			// no error or not important
			case e_Quit:
			case e_Info:
			case e_InterpreterExit:
			case e_RemapColor:
			case e_NeedInput:
			//case e_NeedStdin:
			//case e_NeedStdout:
			//case e_NeedStderr:
			case e_VMreclaim:
			default:
				return true;
		}
	}
	else
	{
		GSError e;
		e.code=code;
		const char* errors[]= { "", ERROR_NAMES };
		int x=(-1)*e.code;
		e.name=errors[x];
		if (locked())//interpreterLock.locked())
			interpreterLock.unlock();
		throw e;
		return false;
	}
}

void GSInterpreterLib::argsToChar()
{
    if (m_argsChar)
    {
        for (int i=0;i<m_argsCCount;i++)
            delete [] *(m_argsChar+i);
        delete [] m_argsChar;
    }
    QStringList internalArgs;
    if (m_args.isEmpty())
    {
        internalArgs << " "
        << "-dMaxBitmap=10000000 "
        << "-dDELAYSAFER"
        << "-dNOPAUSE"
        << "-dNOPAGEPROMPT"
        << QString("-dTextAlphaBits=%1").arg(m_textaa)
		<< QString("-dGraphicsAlphaBits=%1").arg(m_graphaa)
        << QString("-sPAPERSIZE=%1").arg(m_media.toLower())
        << QString().sprintf("-r%dx%d",(int)floor(m_magnify*QX11Info::appDpiX()),(int)floor(m_magnify*QX11Info::appDpiY()));

         if ( !m_pfonts )
             internalArgs << "-dNOPLATFONTS";

    }
    else
        internalArgs = m_args;

    if (m_display)
    {
        internalArgs 
        << QString().sprintf("-dDisplayFormat=%d", DISPLAY_COLORS_RGB | DISPLAY_UNUSED_LAST | DISPLAY_DEPTH_8 |
        DISPLAY_BIGENDIAN | DISPLAY_TOPFIRST)
        << QString().sprintf("-dDisplayHandle=16#%llx", (unsigned long long int) this );
    }


    int t=internalArgs.count();
    char ** args=static_cast <char**> (new char* [t]);
    for (int i=0;i<t;i++)
    {
        *(args+i)=new char [internalArgs[i].length()+1];
        qstrcpy (*(args+i),internalArgs[i].toLocal8Bit());
// 		qDebug("Arg nr %d : %s",i,(internalArgs[i].local8Bit()));
		qDebug("Arg nr %d : %s",i,*(args+i));
    }

    m_argsChar=args;
    m_argsCCount=t;
}

//#include "qgs.moc"
