/***************************************************************************
 *   Copyright (C) 2005 by Piotr Szymanski <niedakh@gmail.com>             *
 *                                                                         *
 * This library is free software; you can redistribute it and/or           *
 * modify it under the terms of the GNU Lesser General Public              *
 * License as published by the Free Software Foundation; either            *
 * version 2.1 of the License, or (at your option) any later version.      *
 *                                                                         *
 * This library is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       *
 * Lesser General Public License for more details.                         *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with this library; if not, write to the Free Software     *
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,                  *
 * MA  02110-1301  USA                                                     *
 ***************************************************************************/

#include <iostream>

#include "qgs.h"
#include "rendering_example.h"
#include <QApplication>
#include <QPoint>
#include <QDialog>
#include <QImage>
#include <QPainter>

#include <QFile>
#include <QString>
#include <QTextStream>

#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <QLabel>
#include <QDebug>

/*
 *	Example of a simple PS renderer, renders only the first page, with very primitive
 *	Document Structure Comments parser, handles both types of updating.
 */

GSInterpreterLib *interpreter;
void PixHandler::slotPixmap(void)
{
	t->exec();
	delete p;
	delete w;
	delete t;
}

void PixHandler::slotPixmap(const QImage *img)
{
	if (!progr)
	{
		//somehow drawing on the  widget didnt work with my qt
		t=new QDialog(0);
		w=new QWidget(t);
 		pix = new QPixmap (img->width(), img->height());
 		pix -> fill();
		w->resize(img->width(),img->height());
		p=new QPainter(pix);
		p->drawImage(0,0,*img,0,0,img->width(),img->height());
		QPalette palette;
		palette.setBrush(w->backgroundRole(), QBrush(*pix));
		w->setPalette(palette);
		w->update();
		t->exec();
		delete p;
		delete pix;
		delete w;
		delete t;
	}
}

void PixHandler::started(const QImage* img)
{
	if (progr)
	{
		image=img;
		t=new QDialog(0);
    	w=new QWidget(t);
    	p=new QPainter(w);
		w->resize(img->width(),img->height());
		QPalette palette;
		palette.setBrush(w->backgroundRole(), QBrush(QColor("white")));
		w->setPalette(palette);
		t->show();
		w->show();
	}
}

void PixHandler::updated(const QRect &r)
{
	if (progr)
	{
		qDebug("Updating: (%d,%d) x (%d,%d)",r.left(),r.top(),r.right(),r.bottom());
		p->drawImage(r.topLeft(),*image,r);
	}
}

PixHandler pixHandler;
FILE * f;

int main (int argc, char* argv[])
{
    // Order of argv: fileName, progressive (0 for false)
    QApplication app(argc,argv);

	GSInterpreterLib::Position prolog, setup, page1;
	int i=0,y;
	if (argc<2)
	{
		qDebug() << "Usage: " << QString(argv[0]) << " filename progressive\n";
		qDebug() << "progressive - progressive rendering (enabled by default) 0 to disable\n";
		return -1;
	}
	// document structure parser
	// the postscript document often has prolog and  setup sections we need
	// to give to ghostscript before page
	QFile file( argv[1] );
    if ( file.open( QIODevice::ReadOnly ) ) 
	{
		QTextStream stream( &file );
		QString s;
		while ( !stream.atEnd() ) 
		{
			s=stream.readLine();
			if (i==0)
			{
				if (s.startsWith("%%EndProlog"))
				{
					qDebug("%ld:\t %s",file.pos(),s.toLatin1().data());
					prolog.second=file.pos();
					setup.first=prolog.second+1;
					i++;
					continue;
				}
			}
			if (i==1)
			{
				if (s.startsWith("%%EndSetup"))
				{
					qDebug("%ld:\t %s",file.pos(),s.toLatin1().data());
					setup.second=file.pos();
					i++;
					continue;
				}
			}
			if (i==2)
			{
				if (s.startsWith("%%Page:"))
				{
					qDebug("%ld:\t %s",file.pos(),s.toLatin1().data());
					page1.first=file.pos();
					i++;
					continue;
				}
			}
			if (i==3)
			{
				if (s.startsWith("%%Page:"))
				{
					qDebug("%ld:\t %s",file.pos(),s.toLatin1().data());
					page1.second=file.pos();
					i++;
					continue;
				}
			}
			if (i==4)
				break;
		}
		y=file.size();
        file.close();
    }
	else
		return -1;

	qDebug("I = %d",i);
	interpreter=new GSInterpreterLib();
    f = fopen ( argv[1] , "r");
	
    interpreter->setMedia ( "a4" );
    interpreter->setMagnify ( 1.0 );
    interpreter->setOrientation ( 0 );
    interpreter->setSize ( 928,1228 );
    interpreter->setAABits(4,2);
	bool progr=true;
	if (argc>=3)
		progr = ( atoi(argv[2]) != 0);

	interpreter->setProgressive(progr);
	pixHandler.setProgressive(progr);

	QObject::connect(interpreter, SIGNAL(Started(const QImage *)),
			&pixHandler, SLOT(started(const QImage *)));
	QObject::connect(interpreter, SIGNAL(Updated(const QRect &)),&pixHandler, SLOT(updated(const QRect &)));
	QObject::connect(interpreter, SIGNAL(Finished()),&pixHandler, SLOT(slotPixmap()));
	QObject::connect(interpreter, SIGNAL(Finished(const QImage *)),
			&pixHandler, SLOT(slotPixmap(const QImage *)));
	interpreter->start(true);
	if (i>=3)
	{
			interpreter->run(f,prolog,false);
			interpreter->run(f,setup,false);
			interpreter->run(f,page1,true);
	}
	else
	{
		page1.first=0;	
		page1.second=y;
		interpreter->run(f,page1,true);
	}
	fclose(f);
	delete interpreter;
    return 0;
}
//#include "rendering_example.moc"

