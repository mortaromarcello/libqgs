/***************************************************************************
 *   Copyright (C) 2005 by Piotr Szymanski <niedakh@gmail.com>             *
 *                                                                         *
 * This library is free software; you can redistribute it and/or           *
 * modify it under the terms of the GNU Lesser General Public              *
 * License as published by the Free Software Foundation; either            *
 * version 2.1 of the License, or (at your option) any later version.      *
 *                                                                         *
 * This library is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       *
 * Lesser General Public License for more details.                         *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with this library; if not, write to the Free Software     *
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,                  *
 * MA  02110-1301  USA                                                     *
 ***************************************************************************/

#ifndef _LIBGS_ASYNC_GENERATOR_
#define _LIBGS_ASYNC_GENERATOR_

#include <QObject>
#include <QRect>

class QImage;
class QDialog;
class QPainter;
class QPixmap;
class QWidget;
class PixHandler : public QObject
{
    Q_OBJECT
	public:
		void setProgressive(bool t=true) { progr=t; };
    public slots:
		void started(const QImage* img);
		void updated(const QRect &r);
        void slotPixmap(void);
        void slotPixmap(const QImage* img);
	private:
		bool progr;
		const QImage* image;
		QDialog *t;
		QWidget *w;
		QPixmap *pix;
		QPainter *p;
};
#endif
